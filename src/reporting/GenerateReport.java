package reporting;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class GenerateReport {
	
	public static void main(String[] args) {
		
		
	    SisenseAndReportManager sisenseManager =SisenseAndReportManager.getInstance();
	    try {
	    	 Calendar c = Calendar.getInstance();
	    	 SimpleDateFormat sformat = new SimpleDateFormat("MMddyyyy");
	    	
			 String filename="Daily_Fee_And_Revenue_Reconciliation_Report_";
			 filename=filename+sformat.format(c.getTime())+".csv";
			 String location="/usr/local/jobs-archive/ReportDeliveryJob/";
			//String location="/Users/sonam/Documents/";
			 
			 
			String json=sisenseManager.getJsonDataFromSisenseForBalance();
			HashMap<String, ArrayList<String>> hp = new HashMap<String, ArrayList<String>>();
					hp=sisenseManager.convertSisenseJsonToMap(json);
			
			String jsonGrid=sisenseManager.getJsonDataFromSisenseForGrid();
			
			HashMap<String, ArrayList<String>> hpGrid =sisenseManager.convertSisenseJsonToMap(jsonGrid);
			
			
			
			filename=sisenseManager.generateCsvFileForFR(hp,hpGrid, filename, location);
			String result=sisenseManager.sendReport(filename);
			if(result!=null){
				System.out.println("File sent");
			}else
				System.out.println("Not sent");
				
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
       
		
		
		
		
	}
	

}
