package reporting;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import com.finxera.reporting.ftpservice.FTPConnectionType;
import com.finxera.reporting.ftpservice.FTPService;
import com.finxera.reporting.ftpservice.FTPServiceImpl;
import com.finxera.reporting.ftpservice.FtpConfig;
import com.google.gson.Gson;

import net.oauth.jsontoken.JsonToken;
import net.oauth.jsontoken.crypto.HmacSHA256Signer;



public class SisenseAndReportManager {

	private static String SISENSE_BASE_URL = null;
	private static Integer SISENSE_BASE_URL_PORT = null;
	private static String DATASOURCE = null;
	private static String KEY = null;
	private static String USERNAME = null;
	private static String PASSWORD = null;
	
	private static String REPORT_FTP_HOSTNAME=null;
	private static String REPORT_FTP_USERNAME=null;
	private static String REPORT_FTP_PASSWORD=null;
	private static String REPORT_FTP_BINARYMODE=null;
	private static String REPORT_REMOTE_DIRECTORY=null;
	private static String REPORT_LOCAL_DIRECTORY=null;
	private static String REPORT_BACKUP_DIRECTORY=null;
	private static String REPORT_CONNECTION_TYPE=null;
	private static String QUERY1=null;
	private static String QUERY2=null;



	private static SisenseAndReportManager instance = new SisenseAndReportManager();

	private SisenseAndReportManager() {
	}

	// Get the only object available
	public static SisenseAndReportManager getInstance() {

		    SISENSE_BASE_URL="http://qa-sisense-master.finxera.com"; // PROD :http://sisense.bancbox.com:8081";
			SISENSE_BASE_URL_PORT=8081;
			DATASOURCE="CFT";
			KEY="b0626f7527f03e7cc572c7a849602393";//PROD -98423a4c6ac5f7ef1e333ab5fb9e4093
			USERNAME="admin@sisense.com";
			PASSWORD="sisense";// "PROD : bbx789BBX";
			
			REPORT_FTP_HOSTNAME="perfftp.cftpay.com";
			REPORT_FTP_USERNAME="test_rpps";
			REPORT_FTP_PASSWORD="oMR05vHJNMAj";
			REPORT_FTP_BINARYMODE="true";
			REPORT_REMOTE_DIRECTORY="/pub";
			REPORT_LOCAL_DIRECTORY="/usr/local/jobs-archive/ReportDeliveryJob/";
			REPORT_BACKUP_DIRECTORY="/usr/local/jobs-archive/ReportDeliveryJob/";
			REPORT_CONNECTION_TYPE="sftp";
			
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, -1);
	    	 SimpleDateFormat sformat = new SimpleDateFormat("yyyyMMdd");
	    	 String date=sformat.format(c.getTime());
	    	
			QUERY1="SELECT OpenBal.Bal AS [Beginning Balance], CASE WHEN CloseBal.Bal IS NULL THEN 0 ELSE CloseBal.Bal END AS [Ending Balance] "
					+ "FROM ( SELECT s.id AS subscribers_id,a.id AS account_type_id FROM subscribers s INNER JOIN subscriber_bank_accounts sba ON sba.subscribers_id = s.id INNER JOIN banks b ON sba.banks_id = b.id INNER JOIN subscriber_accounts sa ON "
					+ "sa.subscriber_bank_accounts_id = sba.id AND sa.subscribers_id = s.id INNER JOIN account_types a ON a.name = 'SUB_FEE' AND a.name = sa. account_type WHERE s.id = 200005 GROUP BY s.id, a.id ) AccountType INNER JOIN ( SELECT entity_id, account_types_id, MAX(balance_date) AS MaxBalDate FROM daily_acct_balances WHERE entity = 1 AND balance_date_int < "+date+" GROUP BY entity_id, account_types_id ) MaxBalanceDate ON MaxBalanceDate.entity_id = AccountType.subscribers_id AND MaxBalanceDate.account_types_id = AccountType.account_type_id INNER JOIN ( SELECT entity_id, account_types_id, MAX(balance) AS Bal, balance_date FROM daily_acct_balances WHERE entity = 1 GROUP BY entity_id, account_types_id, balance_date) OpenBal ON OpenBal.entity_id = MaxBalanceDate.entity_id AND OpenBal.account_types_id = MaxBalanceDate.account_types_id AND OpenBal.balance_date = MaxBalanceDate.MaxBalDate LEFT JOIN ( SELECT entity_id, account_types_id, MAX(balance) AS Bal "
					+ "FROM daily_acct_balances WHERE entity = 1 AND balance_date_int = "+date+" GROUP BY entity_id,account_types_id ) CloseBal "
					+ "ON CloseBal.entity_id = AccountType.subscribers_id AND CloseBal.account_types_id = AccountType.account_type_id";	
			QUERY2="SELECT TRANSACTION_CLASS_id AS [Class] ,CONCAT(CONCAT('''',TOSTRING(account_number)),'''') AS [Client Id],client_name AS [Client Name] ,CASE WHEN ext_client_id IS NULL THEN '' ELSE ext_client_id END AS [CNI Id], TID AS[Transaction Id] ,Scheduled ,Completed ,Amount ,Description FROM sisense_FR WHERE subscriber_id=200005 AND Scheduled_INT = "+date;
			

		return instance;
	}

	public HttpResponse executeQuery(String query)
			throws Exception {

		String url = (SISENSE_BASE_URL + ":" + SISENSE_BASE_URL_PORT
				+ "/api/datasources/" + DATASOURCE + "/sql?query=" + URLEncoder.encode(query,"UTF-8"));
		
		System.out.println(url);

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		String token = null;
		Integer tryCount = 0;
		Integer maxRetries = 2;
		while (tryCount < maxRetries) {
			try {

				token = createTokenForSisense();

				request.setHeader("x-api-key", token);
				HttpResponse response;

				response = client.execute(request);
				if(200!=response.getStatusLine().getStatusCode()){
					
					throw new Exception("Sisense call failed");
				}

				return response;
			} catch (Exception e) {
				tryCount++;
				System.out.println("Exception : " + e);
				if (tryCount >= maxRetries) {
					System.out.println("Max retries exceeded.");
					throw e;
				}
				System.out.println("Retrying sisense call");
			}
		}
		return null;

	}

	public String createTokenForSisense() throws ClientProtocolException,
			IOException, SignatureException {
		System.out.println("In createTokenForSisense");
		Calendar cal = Calendar.getInstance();
		HmacSHA256Signer signer = null;

		try {
			signer = new HmacSHA256Signer("Bancbox", null, KEY.getBytes());
		} catch (InvalidKeyException e1) {
			e1.printStackTrace();
		}

		JsonToken token = new net.oauth.jsontoken.JsonToken(signer);
		token.setIssuedAt(new org.joda.time.Instant(cal.getTimeInMillis()));
		token.setParam("email", USERNAME);
		token.setParam("password", PASSWORD);

		token.serializeAndSign();

		return token.serializeAndSign();

	}
	
	
	  public String getJsonDataFromSisenseForBalance() throws Exception
	    {
	        StringBuffer result = null;
	        SisenseAndReportManager sisenseManager = SisenseAndReportManager.getInstance();

	        HttpResponse response = sisenseManager.executeQuery(QUERY1);
	        if (response == null)
	            return null;
	        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

	        result = new StringBuffer();
	        String line = "";
	        while ((line = rd.readLine()) != null)
	        {
	            result.append(line);
	          
	            System.out.println(line);
	           
	        }
	        if(result.toString().contains("error")){
	        	throw new Exception(result.toString());
	        }
	        return result.toString();
	    }
	  
	  public String getJsonDataFromSisenseForGrid() throws Exception
	    {
	       StringBuffer result = null;
	        SisenseAndReportManager sisenseManager = SisenseAndReportManager.getInstance();

	        HttpResponse response = sisenseManager.executeQuery(QUERY2);
	        if (response == null)
	            return null;
	        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

	        result = new StringBuffer();
	        String line = "";
	        while ((line = rd.readLine()) != null)
	        {
	            result.append(line);
	          
	            System.out.println(line);
	           
	        }
	        if(result.toString().contains("error")){
	        	throw new Exception(result.toString());
	        }
	        return result.toString();
		
	    }

	  
	  public HashMap<String, ArrayList<String>> convertSisenseJsonToMap(String jsonString)
	    {
	       
	        Gson gson = new Gson();
	        HashMap<String, ArrayList<String>> hashmap = new LinkedHashMap<String, ArrayList<String>>();
	        ReportPojo fromJson = gson.fromJson(jsonString, ReportPojo.class);

	        String[] headers2 = fromJson.getHeaders();
	        for (int i = 0; i < headers2.length; i++)
	        {
	            ArrayList<String> values = new ArrayList<String>();
	            for (int j = 0; j < fromJson.getValues().length; j++)
	            {
	                values.add(fromJson.getValues()[j][i]);
	            }
	            hashmap.put(headers2[i], values);
	        }

	        return hashmap;

	    }
	  
	  
	  public String sendReport(String name) throws Exception
	    {
	        System.out.println("In sendReport");
	        /**
	         * Sending via FTP/SFTP based on config CONNECTION_TYPE : ftp/sftp
	         */
	        
	            FTPService ftpService = new FTPServiceImpl();
	            FtpConfig ftpConfig = new FtpConfig();
	            ftpConfig.setHostname(REPORT_FTP_HOSTNAME);
	            ftpConfig.setUsername(REPORT_FTP_USERNAME);
	            ftpConfig.setPassword(REPORT_FTP_PASSWORD);
	            ftpConfig.setBinaryMode(Boolean.parseBoolean(REPORT_FTP_BINARYMODE));
	            ftpConfig.setRemoteDirectory(REPORT_REMOTE_DIRECTORY);
	            ftpConfig.setLocalDirectory(REPORT_LOCAL_DIRECTORY);
	            ftpConfig.setBackupDirectory(REPORT_BACKUP_DIRECTORY);
	            ftpConfig.setConnectionType(FTPConnectionType.getConnectionTypeFromString(REPORT_CONNECTION_TYPE));

	            List<String> filesToSend = new LinkedList<String>();
	            filesToSend.add(name);

	            List<String> putFiles = ftpService.put(ftpConfig, filesToSend);
	            if (putFiles != null && !putFiles.isEmpty())
	            {
	                return "File sent successfully: " + putFiles.get(0);
	            }
				return null;
	        }
	   

	 
	  public String generateCsvFileForFR(Map<String, ArrayList<String>> reportingMapBalance,Map<String, ArrayList<String>> reportingMapGrid, String filename, String location) throws IOException, Exception
	    {
	        System.out.println("In generateCsvFile");
	        Calendar c = Calendar.getInstance();
	        
	        File file = new File(location + filename);

	        StringBuilder csv = new StringBuilder();
	        int row = 0;
	        ArrayList<ArrayList> list = new ArrayList<ArrayList>();

	        // Write the keys row:
	        Iterator keys = reportingMapBalance.keySet().iterator();
	        boolean notFirst = true;
	        while (keys.hasNext())
	        {
	            String key = (String) keys.next();
	            ArrayList tmp = (ArrayList) reportingMapBalance.get(key);
	            if (!notFirst)
	            {
	                csv.append(",");
	            }
	            csv.append(key);
	            // store list
	            list.add(tmp);
	            notFirst = false;
	        }
	        csv.append("\n");
	       
	        //=============== adding beginning & closing balance ==================
	        BigDecimal ob=new BigDecimal("0");
	        BigDecimal clo=new BigDecimal("0");
	        BigDecimal tmp=new BigDecimal("0");

	        ArrayList<String> OpeningBalanceValues = new ArrayList<String>();
	        OpeningBalanceValues=reportingMapBalance.get("Beginning Balance");
	        for(int i=0;i<OpeningBalanceValues.size();i++){
	        	if(OpeningBalanceValues.get(i)!=null){
	        	ob= new BigDecimal (OpeningBalanceValues.get(i));
	        	}
	        }
	        
	        
	        ArrayList<String> ClosingBalanceValues=new ArrayList<String>();
	        ClosingBalanceValues = reportingMapBalance.get("Ending Balance");
	        for(int i=0;i<ClosingBalanceValues.size();i++){
	        	if(ClosingBalanceValues.get(i)!=null){
		        	clo= new BigDecimal (ClosingBalanceValues.get(i));
		        	}
	        }
	       
	        
	        // adding beginning & closing balance
	        csv.append(ob.setScale(2, BigDecimal.ROUND_HALF_UP));
	        csv.append(",");
	        csv.append(clo.setScale(2, BigDecimal.ROUND_HALF_UP));
	        csv.append("\n");
	        
	        //=============== Adding running balance ==================
	        
	        ArrayList<String> AmountValues =  new ArrayList<String>();
	        AmountValues=reportingMapGrid.get("Amount");
	        ArrayList<String> RunningBalance = new ArrayList<String>();
	       
	        for(int i=0;i<AmountValues.size();i++){
	        	
	        	if(AmountValues.get(i)!=null){
	        		 
		        	tmp=ob.add(new BigDecimal(AmountValues.get(i)));
		        	tmp=tmp.setScale(2, BigDecimal.ROUND_HALF_UP);
		        	System.out.println(tmp);
		        	RunningBalance.add(String.valueOf(tmp));
		        	ob=tmp;
		        	
		        	}
	        }
	        
	        //Setting description column at the end
	        ArrayList<String> Description = new ArrayList<String>();
	        Description=reportingMapGrid.get("Description");
	        
	        reportingMapGrid.remove("Description");
	        
	        
	      
	        reportingMapGrid.put("Running balance", RunningBalance);
	        
	        reportingMapGrid.put("Description", Description);
	        
	        
	        //====================== Adding grid values=====================
	        
	        notFirst = true;
	        int row1 = 0;
	        ArrayList<ArrayList> list1 = new ArrayList<ArrayList>();

	        // Write the keys row:
	        Iterator keys1 = reportingMapGrid.keySet().iterator();
	       
	        while (keys1.hasNext())
	        {
	            String key1 = (String) keys1.next();
	            ArrayList tmp1 = (ArrayList) reportingMapGrid.get(key1);
	            if (!notFirst)
	            {
	                csv.append(",");
	            }
	            csv.append(key1);
	            // store list
	            list1.add(tmp1);
	            notFirst = false;
	        }
	        csv.append("\n");

	        // Write the rest of the rows
	        while (row1 < list1.get(0).size())
	        {
	            notFirst = true;
	            for (int x = 0; x < list1.size(); x++)
	            {
	                if (!notFirst)
	                {
	                    csv.append(",");
	                }
	                String token = (String) list1.get(x).get(row1);
	                if (token.contains(","))
	                {
	                    token = token.replace(",", " ");
	                }
	                csv.append(token);
	                // csv.append((String) list.get(x).get(row));
	                notFirst = false;
	            }
	            csv.append("\n");
	            row1++;
	        }
	        
	        
	 

	        FileUtils.writeStringToFile(file, csv.toString());

	        return file.getName();
	    }
	    

}
