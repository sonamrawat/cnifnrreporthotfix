package com.finxera.reporting.ftpservice;


	public class FTPServiceException extends Exception {

	    private static final long serialVersionUID = 1L;

	    public FTPServiceException(String message) {
	        super(message);
	    }
	    
	    public FTPServiceException(Exception e) {
	        super(e);
	    }
	}


