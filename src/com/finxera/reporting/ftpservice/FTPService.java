package com.finxera.reporting.ftpservice;

import java.io.File;
import java.util.List;

public interface FTPService {
	List<File> get(FtpConfig ftpConfig, String pattern, boolean deleteRemote)
			throws FTPServiceException;

	List<String> put(FtpConfig ftpConfig, List<String> filenames)
			throws FTPServiceException;

}
