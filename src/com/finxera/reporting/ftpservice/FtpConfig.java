package com.finxera.reporting.ftpservice;

import java.io.Serializable;

	public class FtpConfig implements Serializable
	{
	    private static final long serialVersionUID = 1L;
	    private String hostname;
	    private String username;
	    private String password;
	    private String remoteDirectory;
	    private String localDirectory;
	    private boolean binaryMode;
	    private boolean createRemoteDir = false;
	    private String backupDirectory;
	    private String batchRmoteLogDirectoryName;
	    private String batchRmoteErrorDirectoryName;
	    private String batchRmoteImgDirectoryName;
	    private String remoteBacthFilesDirectoryName;
	    private String remoteImgFilesDirectoryName;
	    private String processedFilesDirectoryName;
	    private String mailAddress;
	    private String sftpUserKey;
	    private FTPConnectionType connectionType;

	    /**
	     * @return the FTP host
	     */
	    public String getHostname()
	    {
	        return this.hostname;
	    }

	    /**
	     * @param hostname
	     *            the FTP host
	     */
	    public void setHostname(String hostname)
	    {
	        this.hostname = hostname;
	    }

	    /**
	     * @return the user name;
	     */
	    public String getUsername()
	    {
	        return this.username;
	    }

	    /**
	     * @param username
	     *            the user name for login
	     */
	    public void setUsername(String username)
	    {
	        this.username = username;
	    }

	    /**
	     * @return the password
	     */
	    public String getPassword()
	    {
	        return this.password;
	    }

	    /**
	     * @param password
	     *            the password
	     */
	    public void setPassword(String password)
	    {
	        this.password = password;
	    }

	    /**
	     * @return the remote directory
	     */
	    public String getRemoteDirectory()
	    {
	        return this.remoteDirectory;
	    }

	    /**
	     * @param remoteDirectory
	     *            the remote directory
	     */
	    public void setRemoteDirectory(String remoteDirectory)
	    {
	        this.remoteDirectory = remoteDirectory;
	    }

	    /**
	     * @return the local directory
	     */
	    public String getLocalDirectory()
	    {
	        return this.localDirectory;
	    }

	    /**
	     * @param localDirectory
	     *            the local directory
	     */
	    public void setLocalDirectory(String localDirectory)
	    {
	        this.localDirectory = localDirectory;
	    }

	    /**
	     * @return the {@code true} if to transfer using binary mode, {@code false}
	     *         otherwise
	     */
	    public boolean isBinaryMode()
	    {
	        return this.binaryMode;
	    }

	    /**
	     * @param binaryMode
	     *            {@code true} if to transfer using binary mode, {@code false}
	     *            otherwise
	     */
	    public void setBinaryMode(boolean binaryMode)
	    {
	        this.binaryMode = binaryMode;
	    }

	    /**
	     * @return the backup directory
	     */
	    public String getBackupDirectory()
	    {
	        return this.backupDirectory;
	    }

	    /**
	     * @param backupDirectory
	     *            the directory where to move files after transfer
	     */
	    public void setBackupDirectory(String backupDirectory)
	    {
	        this.backupDirectory = backupDirectory;
	    }

	    /**
	     * @return the batchRmoteLogDirectoryName
	     */
	    public String getBatchRmoteLogDirectoryName()
	    {
	        return this.batchRmoteLogDirectoryName;
	    }

	    /**
	     * @param batchRmoteLogDirectoryName
	     *            the batchRmoteLogDirectoryName to set
	     */
	    public void setBatchRmoteLogDirectoryName(String batchRmoteLogDirectoryName)
	    {
	        this.batchRmoteLogDirectoryName = batchRmoteLogDirectoryName;
	    }

	    /**
	     * @return the batchRmoteErrorDirectoryName
	     */
	    public String getBatchRmoteErrorDirectoryName()
	    {
	        return this.batchRmoteErrorDirectoryName;
	    }

	    /**
	     * @param batchRmoteErrorDirectoryName
	     *            the batchRmoteErrorDirectoryName to set
	     */
	    public void setBatchRmoteErrorDirectoryName(String batchRmoteErrorDirectoryName)
	    {
	        this.batchRmoteErrorDirectoryName = batchRmoteErrorDirectoryName;
	    }

	    /**
	     * @return the batchRmoteImgDirectoryName
	     */
	    public String getBatchRmoteImgDirectoryName()
	    {
	        return this.batchRmoteImgDirectoryName;
	    }

	    /**
	     * @param batchRmoteImgDirectoryName
	     *            the batchRmoteImgDirectoryName to set
	     */
	    public void setBatchRmoteImgDirectoryName(String batchRmoteImgDirectoryName)
	    {
	        this.batchRmoteImgDirectoryName = batchRmoteImgDirectoryName;
	    }

	    public String getRemoteBacthFilesDirectoryName()
	    {
	        return this.remoteBacthFilesDirectoryName;
	    }

	    public void setRemoteBacthFilesDirectoryName(String remoteBacthFilesDirectoryName)
	    {
	        this.remoteBacthFilesDirectoryName = remoteBacthFilesDirectoryName;
	    }

	    public String getRemoteImgFilesDirectoryName()
	    {
	        return this.remoteImgFilesDirectoryName;
	    }

	    public void setRemoteImgFilesDirectoryName(String remoteImgFilesDirectoryName)
	    {
	        this.remoteImgFilesDirectoryName = remoteImgFilesDirectoryName;
	    }

	    public String getProcessedFilesDirectoryName()
	    {
	        return this.processedFilesDirectoryName;
	    }

	    public void setProcessedFilesDirectoryName(String processedFilesDirectoryName)
	    {
	        this.processedFilesDirectoryName = processedFilesDirectoryName;
	    }

	    @Override
	    protected Object clone() throws CloneNotSupportedException
	    {
	        // TODO Auto-generated method stub
	        return super.clone();
	    }

	    public String getMailAddress()
	    {
	        return this.mailAddress;
	    }

	    public void setMailAddress(String mailAddress)
	    {
	        this.mailAddress = mailAddress;
	    }

	    public boolean isCreateRemoteDir()
	    {
	        return this.createRemoteDir;
	    }

	    public void setCreateRemoteDir(boolean createRemoteDir)
	    {
	        this.createRemoteDir = createRemoteDir;
	    }

	    /**
	     * @return the sftpUserKey
	     */
	    public String getSftpUserKey()
	    {
	        return this.sftpUserKey;
	    }

	    /**
	     * @param sftpUserKey
	     *            the sftpUserKey to set
	     */
	    public void setSftpUserKey(String sftpUserKey)
	    {
	        this.sftpUserKey = sftpUserKey;
	    }

	    public FTPConnectionType getConnectionType() {
	        // Default is sftp
	        if(connectionType == null) {
	            return FTPConnectionType.SFTP;
	        }
	        return connectionType;
	    }

	    public void setConnectionType(FTPConnectionType connectionType) {
	        this.connectionType = connectionType;
	    }

	}


