package com.finxera.reporting.ftpservice;


	public enum FTPConnectionType {
	    FTP("ftp"),
	    SFTP("sftp");
	    private String connectionType;

	    FTPConnectionType(String connectionType) {
	        this.connectionType = connectionType;
	    }

	    private String getConnectionType() {
	        return connectionType;
	    }

	    public static FTPConnectionType getConnectionTypeFromString(String connectionType) {
	        if (connectionType != null && !connectionType.isEmpty()) {
	            for(FTPConnectionType connectionTypeEnum : FTPConnectionType.values()) {
	                if (connectionTypeEnum.getConnectionType().equals(connectionType)) {
	                    return connectionTypeEnum;
	                }
	            }
	        }
	        return SFTP;
	    }
	}



