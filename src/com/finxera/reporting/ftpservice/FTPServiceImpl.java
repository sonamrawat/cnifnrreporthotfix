package com.finxera.reporting.ftpservice;

import static com.finxera.reporting.ftpservice.FTPConnectionType.FTP;
import static com.finxera.reporting.ftpservice.FTPConnectionType.SFTP;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.apache.commons.vfs2.UserAuthenticator;
import org.apache.commons.vfs2.auth.StaticUserAuthenticator;
import org.apache.commons.vfs2.impl.DefaultFileSystemConfigBuilder;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;



public class FTPServiceImpl implements FTPService {
	  private Log logger = LogFactory.getLog(getClass());

	@Override
	public List<File> get(FtpConfig ftpConfig, String pattern,
			boolean deleteRemote) throws FTPServiceException {
				return null;
		
	}

	@Override
	public List<String> put(FtpConfig ftpConfig, List<String> filenames)
			throws FTPServiceException {
        if (ftpConfig.getConnectionType().equals(FTP)) {
            return putFTP(ftpConfig, filenames);
        } else if (ftpConfig.getConnectionType().equals(SFTP)) {
            return putSFTP(ftpConfig, filenames);
        }
        throw new FTPServiceException("Connection type : \'" + ftpConfig.getConnectionType()
                + "\' not supported for put");
	}

	private List<String> putFTP(FtpConfig ftpConfig, List<String> filenames) throws FTPServiceException {
        FTPClient ftp = new FTPClient();
        List<String> successfullySent = new LinkedList<String>();
        try {
            if (connectToFtp(ftpConfig, ftp)) {
                // Do actual work
                for(String filename : filenames) {
                    /*
                     * To make sure that if one file fails, we can proceed with
                     * the other files
                     */
                    FileInputStream fis = null;
                    try {
                        File fileToSend = new File(ftpConfig.getLocalDirectory(), filename);
                        fis = new FileInputStream(fileToSend);
                       
                        System.out.println("Sending file: " + filename);
                        if (ftp.storeFile(filename, fis)) {
                            successfullySent.add(filename);
                            System.out.println("File successfully sent");
                            if (ftpConfig.getBackupDirectory() != null) {
                                System.out.println("Copying file to backup directory");
                                File backupDir = new File(ftpConfig.getBackupDirectory());
                                FileUtils.copyFileToDirectory(fileToSend, backupDir, true);
                                
                                System.out.println("File has been copied to backup directory");
                            }
                        } else {
                            
                            System.out.println("Failed to send file: " + filename);
                        }
                    }
                    catch (Exception e) {
                       
                        System.out.println(e.getMessage());
                    }
                    finally {
                        if (fis != null) {
                            try {
                                fis.close();
                            }
                            catch (IOException e) {
                                
                                System.out.println(e.getMessage());
                            }
                        }
                    }
                }
                ftp.logout();
            }
        }
        catch (IOException e) {
        
            System.out.println(e.getMessage());
        }
        finally {
            if (ftp.isConnected()) {
                try {
                    ftp.disconnect();
                }
                catch (IOException e) {
             
                    System.out.println(e.getMessage());
                }
            }
        }
        return successfullySent;
    }
	
	 private List<String> putSFTP(FtpConfig ftpConfig, List<String> filenames) {
	        List<String> successfullySent = new LinkedList<String>();
	        StandardFileSystemManager manager = new StandardFileSystemManager();
	        try {
	            String sftpUri = generateSftpUri(ftpConfig);
	          
	            System.out.println("sftUri = " + sftpUri);
	            FileSystemOptions opts = new FileSystemOptions();
	            UserAuthenticator auth = new StaticUserAuthenticator(null, ftpConfig.getUsername(), ftpConfig.getPassword());
	            DefaultFileSystemConfigBuilder.getInstance().setUserAuthenticator(opts, auth);
	            if (StringUtils.isNotBlank(ftpConfig.getSftpUserKey())) {
	                File f = new File(ftpConfig.getSftpUserKey());
	               
	                System.out.println("sftpUserKey = " + f.getAbsolutePath());
	                SftpFileSystemConfigBuilder.getInstance().setIdentities(opts, new File[] { f });
	            }
	            SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
	            manager.init();
	            for(String filename : filenames) {
	                String fileToResolve = genrateFileToResovle(sftpUri, ftpConfig.getRemoteDirectory(), filename);
	                
	                System.out.println("fileToResolve = " + fileToResolve);
	                FileObject fileObject = manager.resolveFile(fileToResolve, opts);
	                File fileToSend = new File(ftpConfig.getLocalDirectory(), filename);
	               
	                System.out.println("fileToSend = " + fileToSend.getAbsolutePath());
	                FileObject localFileObject = manager.resolveFile(fileToSend.getAbsolutePath());
	                fileObject.copyFrom(localFileObject, Selectors.SELECT_SELF);
	                successfullySent.add(filename);
	                this.logger.info("file successfully sent");
	            }
	        }
	        catch (Exception e) {
	        	
	           this.logger.error(e.getMessage(), e);
	        }
	        finally {
	            manager.close();
	        }
	        return successfullySent;
	    }
	 
	 private boolean connectToFtp(FtpConfig ftpConfig, FTPClient ftp) throws SocketException, IOException {
	        int reply;
	        String hostname = extractHost(ftpConfig.getHostname());
	        int port = extractPort(ftpConfig.getHostname());
	        if (port > 0) {
	            ftp.setDefaultPort(port);
	        }
	        this.logger.debug("Connecting to " + hostname + " port " + ftp.getDefaultPort());
	        System.out.println("Connecting to " + hostname + " port " + ftp.getDefaultPort());
	        ftp.connect(hostname);
	        this.logger.debug("Connected to " + hostname + " port " + ftp.getDefaultPort());
	        System.out.println("Connected to " + hostname + " port " + ftp.getDefaultPort());
	        this.logger.debug(ftp.getReplyString());
	        // After connection attemp, we should check the reply code to verify
	        // success.
	        reply = ftp.getReplyCode();
	        if (!FTPReply.isPositiveCompletion(reply)) {
	            this.logger.error("FTP server refused connection.");
	            System.out.println("FTP server refused connection.");
	            ftp.disconnect();
	            return false;
	        }
	        this.logger.debug("Connection successful");
	        System.out.println("Connection successful");
	        ftp.enterLocalPassiveMode();
	        ftp.setRemoteVerificationEnabled(false);
	        if (!ftp.login(ftpConfig.getUsername(), ftpConfig.getPassword())) {
	            this.logger.error("Login unsuccessful. The username/password combination is invalid.");
	            ftp.disconnect();
	            return false;
	        }
	        this.logger.debug("Login successful");
	        if (ftpConfig.getRemoteDirectory() != null) {
	            this.logger.debug("Changing to target remote directory: " + ftpConfig.getRemoteDirectory());
	            System.out.println("Changing to target remote directory: " + ftpConfig.getRemoteDirectory());
	            if (!ftp.changeWorkingDirectory(ftpConfig.getRemoteDirectory())) {
	                if (ftpConfig.isCreateRemoteDir()) {
	                    if (!ftp.makeDirectory(ftpConfig.getRemoteDirectory())) {
	                        this.logger.error("Failed to create new remote directory: " + ftpConfig.getRemoteDirectory());
	                        ftp.disconnect();
	                        return false;
	                    }
	                    if (!ftp.changeWorkingDirectory(ftpConfig.getRemoteDirectory())) {
	                        this.logger.error("Failed to change to newly created target remote directory: "
	                                + ftpConfig.getRemoteDirectory());
	                        ftp.disconnect();
	                        return false;
	                    }
	                } else {
	                    this.logger.error("Failed to change to target remote directory: " + ftpConfig.getRemoteDirectory());
	                    ftp.disconnect();
	                    return false;
	                }
	            }
	            this.logger.debug("Change remote directory successful");
	        } else {
	            this.logger.debug("Not changing target remote directory");
	        }
	        if (ftpConfig.isBinaryMode()) {
	            if (!ftp.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE)) {
	                this.logger.error("Failed to change to BINARY file type");
	                ftp.disconnect();
	                return false;
	            }
	            this.logger.debug("Change to binary transfer mode successful");
	        } else {
	            if (!ftp.setFileType(org.apache.commons.net.ftp.FTP.ASCII_FILE_TYPE)) {
	                this.logger.error("Failed to change to ASCII file type");
	                ftp.disconnect();
	                return false;
	            }
	            this.logger.debug("Changed to ascii transfer mode successful");
	        }
	        return true;
	    }

	 /**
	     * @param ftpConfig
	     * @return
	     */
	    private String generateSftpUri(FtpConfig ftpConfig) {
	        StringBuilder str = new StringBuilder();
	        str.append("sftp://").append(ftpConfig.getHostname());
	        if (ftpConfig.getRemoteDirectory() != null) {
	            if (!ftpConfig.getRemoteDirectory().startsWith("/")) {
	                str.append("/");
	            }
	            str.append(ftpConfig.getRemoteDirectory());
	        }
	        return str.toString();
	    }
	    /**
	     * @param sftpUri
	     * @param remoteDirectory
	     * @param filename
	     * @return
	     */
	    private String genrateFileToResovle(String sftpUri, String remoteDirectory, String filename) {
	        StringBuilder str = new StringBuilder();
	        str.append(sftpUri);
	        /*
	         * if ((remoteDirectory != null) && !remoteDirectory.startsWith("/")) {
	         * str.append("/"); } if (remoteDirectory != null) {
	         * str.append(remoteDirectory); }
	         */
	        if ((filename != null) && !str.toString().endsWith("/")) {
	            str.append("/");
	        }
	        if (filename != null) {
	            str.append(filename);
	        }
	        return str.toString();
	    }
	    
	    

	    /**
	     * @param hostname
	     * @return
	     */
	    private String extractHost(String hostname) {
	        if (hostname == null) {
	            return null;
	        }
	        if (hostname.contains(":")) {
	            return hostname.substring(0, hostname.indexOf(":"));
	        }
	        return hostname;
	    }

	    /**
	     * @param hostname
	     * @return
	     */
	    private int extractPort(String hostname) {
	        int result = 0;
	        if (hostname == null) {
	            return result;
	        }
	        if (hostname.contains(":")) {
	            if ((hostname.indexOf(":") + 1) < hostname.length()) {
	                String tmp = hostname.substring(hostname.indexOf(":") + 1);
	                try {
	                    result = Integer.parseInt(tmp);
	                }
	                catch (NumberFormatException e) {
	                    this.logger.error("Unable to parse port from hostname: " + hostname, e);
	                    result = 0;
	                }
	            }
	        }
	        return result;
	    }
}
